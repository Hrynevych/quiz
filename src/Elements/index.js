import Logo from "./Logo";
import Category from "./Category";
import GradientLogo from "./GradientLogo"
const Elements = {
  Logo,
  Category,
  GradientLogo
}

export default Elements;