import allCategoriesData from "../../Assets/mock-data/ctegoriesData";

interface Props {
    categoryName: string
}
const Category = ({categoryName}:Props) => {
    return (
        <div className={"category category--background"}>
            {allCategoriesData.map((element) => {
                if (element.name === categoryName) {
                    return (
                        <div key={element.id} className="category__item">
                            <div className={"category__logo "}>
                                <img src={element.logo} alt="" />
                            </div>
                            <div className={"category__horizontal-line"}>
                                <hr />
                            </div>
                            <div className={"category__title"}>
                                <p>{element.text}</p>
                            </div>
                        </div>
                    );
                }
                return false;
            })}
        </div>
    );
};
export default Category;
