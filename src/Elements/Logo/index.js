import logo from '../../Assets/img/qlogo.png';
import quizlogo from '../../Assets/img/QuizLogo.png';
const Logo = () => {
  return(
    <div className='logo'>
      <div className='logo__item'>
        <div className='logo__q-logo logo__q-logo--width-size '>
          <img className='logo__q-img' src={logo} alt='Qlofo' />
        </div>
        <div className='logo__quiz-logo logo__quiz-logo--width-size-m'>
          <img className='logo__quiz-img' src={quizlogo} alt='Qlofo'/>
        </div>
      </div>
    </div>
  )
}

export default Logo;