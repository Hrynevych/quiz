import {Routes, Route} from "react-router-dom";
import Coponents from "./Components/index";
import allCategoriesData from "./Assets/mock-data/ctegoriesData";
import "./styles/themes/default/themes.scss";

function App() {
    const {Intro, SelectedCategory, Quiz, Score} = Coponents;
    return (
        <div className="App">
            <Routes>
                {allCategoriesData.map((element) => (
                    <Route key={element.id} path="">
                        <Route path="" element={<Intro />} />
                        <Route path="">
                            <Route path={element.name}>
                                <Route
                                    path=""
                                    element={
                                        <SelectedCategory categoryName={element.name} />
                                    }
                                />
                                <Route
                                    path="quiz"
                                    element={<Quiz categoryQuestions={element.name} />}
                                />
                                <Route
                                    path="score"
                                    element={<Score categoryScore={element.name} />}
                                />
                            </Route>
                        </Route>
                    </Route>
                ))}
            </Routes>
        </div>
    );
}
export default App;
