import {Link} from "react-router-dom";
import arrowPLay from "../../Assets/img/arrowPlay.png";
interface Props {
    categoryName:string;
    text:string;

}
const StartButton = ({categoryName,text}:Props) => {
    return (
        <Link to={"/" + categoryName + "/quiz"}>
            <div className="button">
                <h1>{text}</h1>

                <img
                    src={arrowPLay}
                    alt="Start"
                    onClick={() => {
                        localStorage.setItem(categoryName, '');
                    }}
                />
            </div>
        </Link>
    );
};

export default StartButton;
