import React from "react";
import Category from "../../Elements/Category";
import {Link} from "react-router-dom";
import allCategoriesData from "../../Assets/mock-data/ctegoriesData";

interface Props {
    filter: string;
}

const Categories = ({filter}: Props) => {
    const filteredCategories = allCategoriesData.filter((Arr) =>
        filter ? Arr.name !== filter : Arr,
    );

    return (
        <div className="categories categories--direction">
            {filteredCategories.map((element) => (
                <Link key={element.id} to={`/${element.name}`}>
                    <Category categoryName={element.name} />
                </Link>
            ))}
        </div>
    );
};

export default Categories;
