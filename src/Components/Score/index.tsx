import Element from "../../Elements/index";
import Component from "../index";
import xLogo from "../../Assets/img/xLogo.png";
import arrowLogo from "../../Assets/img/arrowLogo.png";
import {useNavigate} from "react-router-dom";

interface Props {
    categoryScore:string;
}
const Score = ({categoryScore}:Props) => {
    let navigate = useNavigate();
    const {Logo, Category, GradientLogo} = Element;
    const {StartButton, Counter, Categories} = Component;
    const score = localStorage.getItem(categoryScore);
    return (
        <div className="score-page">
            <div className="score-page__item">
                <GradientLogo />
                <Logo />
                <div className="score-page__quick-nav">
                    <img
                        onClick={() => navigate(-1)}
                        src={arrowLogo}
                        alt="arrowlogo"
                    />
                    <img
                        src={xLogo}
                        alt="xlogo"
                        onClick={() => navigate("/")}
                    />
                </div>
                <Category categoryName={categoryScore} />
                <Counter numberOfQuestions={undefined} quantity={undefined} score={score} text="Result"></Counter>
                <StartButton text="Repeat Quiz" categoryName={categoryScore} />
            </div>
            <nav className="score-page__nav">
                <Categories filter={categoryScore} />
            </nav>
        </div>
    );
};
export default Score;
