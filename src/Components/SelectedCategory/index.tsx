import Elements from "../../Elements";
import Components from "../index";

interface Props {
    categoryName:string;
}
const SelectedCategory = ({categoryName}:Props) => {
    const {Title,StartButton} = Components;
    const {Category} = Elements;
    return (
        <div className="selected-category">
            <Title title='Select ' text=""/>
            <div className="selected-category__item">
                <Category categoryName={categoryName} />
                <StartButton text='Start' categoryName={categoryName}/>
            </div>
        </div>
    );
};

export default SelectedCategory;
