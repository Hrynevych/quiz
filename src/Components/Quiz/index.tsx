import React, {useState} from "react";
import Components from "../index";
import questionsJson from "../../Assets/mock-data/qestionData.json";
import {useNavigate} from "react-router-dom";

interface Props {
    categoryQuestions: keyof typeof questionsJson;
}

const Quiz = ({categoryQuestions}: Props): JSX.Element => {
    let navigate = useNavigate();
    const {Title, Questions, Counter, DragAndDrop} = Components;
    const [numberQuestion, setNumberQuestion] = useState<number>(1);
    const [score, setScore] = useState<number>(0);
    const numberOfQuestions = questionsJson[categoryQuestions].length;

    const handleAnswer = (isCorrect: boolean): void => {
      
        if (isCorrect === true) {
            setScore(score + 1);
            localStorage.setItem(categoryQuestions, score.toString()  );
            
        }
        if (numberQuestion === 10) {
            navigate("/" + categoryQuestions + "/score");
        }
        setNumberQuestion(numberQuestion + 1);
    };

    return (
        <div className="quiz-page">
            <Title title="SELECT THE CORRECT ANSWER" text="" />
            <div className="quiz-page__counter">
                <Counter
                    text=""
                    score={undefined}
                    numberOfQuestions={numberQuestion}
                    quantity={numberOfQuestions}
                />
            </div>
            {numberQuestion % 2 === 0 ? (
                <DragAndDrop
                    categoryQuestions={categoryQuestions}
                    numberQuestion={numberQuestion}
                    handleAnswer={handleAnswer}
                ></DragAndDrop>
            ) : (
                <Questions
                    categoryQuestions={categoryQuestions}
                    numberQuestion={numberQuestion}
                    handleAnswer={handleAnswer}
                />
            )}
        </div>
    );
};
export default Quiz;
