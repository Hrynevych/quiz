import React from "react";
import Title from "../Title";
import Categories from "../Categories/index";
export const Intro = (): JSX.Element => {
    return (
        <div className="intro">
            <Title
                title="10 QUESTIONS / 5 CATEGORIES"
                text="CHOOSE A CATEGORY"
            />
            <Categories filter="" />
        </div>
    );
};
