import questionsJson from "../../Assets/mock-data/qestionData.json";

const Questions = ({categoryQuestions, numberQuestion, handleAnswer}) => {
    const questionData = JSON.parse(JSON.stringify(questionsJson));
    const arreyNumberQuestion = numberQuestion - 1;
    return (
        <div className="questions">
            <div className="questions__title">
                <h1>
                    {
                        questionData[categoryQuestions][arreyNumberQuestion]
                            .questionText
                    }
                </h1>
            </div>
            <div className="questions__item">
                {questionData[categoryQuestions][
                    arreyNumberQuestion
                ].answerOptions.map((element) => (
                    <button
                        key={element.id}
                        className="questions__button"
                        onClick={() => handleAnswer(element.isCorrect)}
                    >
                        {element.answerText}
                    </button>
                ))}
            </div>
        </div>
    );
};

export default Questions;
