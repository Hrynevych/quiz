interface Props{
    numberOfQuestions?:number;
    score?:string | null;
    quantity?:number;
    text:string;

}
const Counter = ({numberOfQuestions,score, quantity, text}:Props) => {
    return (
        <div className="counter  counter--size">
            <div className="counter__text">
                {text ? <h1>{text}</h1> : ""}
                <div className="counter__p">
                    <p>{numberOfQuestions? numberOfQuestions : ''}</p>
                    <p>{score? score : ''}</p>
                    <p>/</p>
                    <p className="counter__quantity">
                        {quantity ? quantity : 10}
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Counter;
