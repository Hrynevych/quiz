import {Droppable,Draggable} from "react-beautiful-dnd";
interface Props{
    title?:string;
    list?: any[];

}
const Container = ({title, list = []}:Props):JSX.Element => {
    return (
        <div className="container">
            <h2 className="container__title">{title}</h2>
            <Droppable droppableId="list" >
                {(provided) => (
                    <div
                        ref={provided.innerRef}
                        {...provided.droppableProps}
                        className="container__item"
                    >
                        {list.map((element:{id:number , answerText:string}, index) => (
                            <Draggable key={element.id}  draggableId={element.answerText} index={index}>
                                {(provided) => (
                                    <div
                                        {...provided.dragHandleProps}
                                        {...provided.draggableProps}
                                        ref={provided.innerRef}
                                        
                                        className="container__choice"
                                    >
                                        {element.answerText}
                                    </div>
                                )}
                                
                            </Draggable>
                        ))}
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        </div>
    );
};

export default Container;
