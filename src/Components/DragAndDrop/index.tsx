import {DragDropContext, DropResult} from "react-beautiful-dnd";
import React, {useEffect, useState} from "react";
import Container from "./Container";
import questionsJson from "../../Assets/mock-data/qestionData.json";
import Choice from "./Сhoice";

interface Props {
    categoryQuestions: keyof typeof questionsJson;
    handleAnswer(isCorrect: boolean): void;
    numberQuestion: number;
}
interface Answer {
    id: number;
    answerText: string;
    isCorrect: boolean;
}
interface Questions {
    questionText: string;
    answerOptions: [Answer];
}

const DragAndDrop = ({
    categoryQuestions,
    numberQuestion,
    handleAnswer,
}: Props): JSX.Element => {
    const numberQuestionInArr: number = numberQuestion - 1;
    const [groupOfQuestions, setGroupOfQuestions] = useState<Questions | undefined>(undefined);
    const [answer, setAnswer] = useState<Answer | undefined>(undefined);
    const mockData:any = questionsJson[categoryQuestions][numberQuestionInArr];
    
    const onDragStart = (result: {
        source: {droppableId: string; index: number};
    }) => {
        let {source} = result;
        if (source.droppableId === "list" && answer !== undefined) {
        }
    };
    const onDragEnd = (result: DropResult): void => {
        let {destination, source} = result;
        let dragElement = groupOfQuestions!.answerOptions[source.index];
        const list = groupOfQuestions?.answerOptions;
        if (!destination) {
            return;
        }
        if (source.droppableId === destination.droppableId) {
            list?.splice(source.index, 1);
            list?.splice(destination.index, 0, dragElement);
        }
        if (
            source.droppableId === "list" &&
            destination.droppableId === "choice"
        ) {
            list?.splice(source.index, 1);
            setAnswer(dragElement);
        }
        if (
            source.droppableId === "list" &&
            destination.droppableId === "choice" &&
            answer !== undefined
        ) {
            const list = groupOfQuestions?.answerOptions;
            list?.splice(destination.index, 0, answer);
            setAnswer(dragElement);
        }
        if (
            source.droppableId === "choice" &&
            destination.droppableId === "list" &&
            answer
        ) {
            list?.splice(destination.index, 0, answer);
            setAnswer(undefined);
        }
    };
    const clear = () => {
        setAnswer(undefined);
    };
    useEffect(() => {
        setGroupOfQuestions(mockData);
    }, [categoryQuestions, numberQuestionInArr,mockData]);
    return (
        <div className="dragndrop">
            <DragDropContext onDragEnd={onDragEnd} onDragStart={onDragStart}>
                <Container
                    title={groupOfQuestions?.questionText}
                    list={groupOfQuestions?.answerOptions}
                />
                <Choice
                    choice={answer}
                    handleAnswer={handleAnswer}
                    clear={clear}
                />
            </DragDropContext>
        </div>
    );
};

export default DragAndDrop;
