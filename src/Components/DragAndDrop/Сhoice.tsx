import {Droppable, Draggable} from "react-beautiful-dnd";
interface Props {
    choice?:{isCorrect?:boolean ,answerText?:string};
    handleAnswer(choice?:{}): void;
    clear(): void;

}
const Choice = ({choice, handleAnswer, clear}:Props):JSX.Element => {
    const nextQuestion = () => {
        handleAnswer(choice?.isCorrect);
        clear();
    };
    return (
        <div className="choice">
           <h2>Drag Here</h2>
            <Droppable
                droppableId="choice"
            >
                {(provided) => (
                    <div
                        ref={provided.innerRef}
                        {...provided.droppableProps}
                        className="choice__item"
                    >   
                        <Draggable
                            draggableId="item"
                            isDragDisabled={!choice ?? false}
                            index={0}
                            
                            
                            
                            
                        >
                            {(provided) => (
                                <div
                                    {...provided.dragHandleProps}
                                    {...provided.draggableProps}
                                    ref={provided.innerRef}
                                    className={`choice__answer ${choice ? 'choice__answer--active': 'choice__answer--disable'}`}
                                >
                                    {choice?.answerText}
                                </div>
                            )}
                        </Draggable>
                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
            <button onClick={() => nextQuestion()}>Next</button>
        </div>
    );
};

export default Choice;
