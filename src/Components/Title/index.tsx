import React from "react";
import Elements from "../../Elements";
import xLogo from "../../Assets/img/xLogo.png";
import arrowLogo from "../../Assets/img/arrowLogo.png";
import {useNavigate} from "react-router-dom";
interface Props {
    title: string;
    text:string;
}
const Title = ({title, text}:Props) => {
    const {Logo, GradientLogo} = Elements;
    let navigate = useNavigate();
    return (
        <div className="title">
            <GradientLogo />
            <Logo/>

            <div className="title__item">
                <img
                    onClick={() => navigate(-1)}
                    src={arrowLogo}
                    alt="arrowlogo"
                />
                <img 
                    src={xLogo} 
                    alt="xlogo" 
                    onClick={() => navigate("/")} />
            </div>
            <div className="title__text">
                <div
                    className={`title__heading ${
                        title
                            ? "title__heading--background"
                            : "title__text-item"
                    }`}
                >
                    <h1>{title}</h1>
                </div>

                {text ? <p>{text}</p>: undefined}
            </div>
        </div>
    );
};

export default Title;
