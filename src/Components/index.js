import {Intro} from "../Components/Intro/index";
import SelectedCategory from "./SelectedCategory";
import Quiz from "./Quiz/index";
import Score from "./Score";
import Categories from "./Categories/index";
import Title from "./Title";
import StartButton from "./StartButton";
import Questions from "./Questions";
import Counter from "./Сounter";
import DragAndDrop from "./DragAndDrop";

const Components = {
  Intro,
  Quiz,
  SelectedCategory,
  Score,
  Categories,
  Title,
  StartButton,
  Questions,
  Counter,
  DragAndDrop
}

export default Components;