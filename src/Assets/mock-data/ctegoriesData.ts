import TechnologyLogo from "../../Assets/img/technolog_logo.png";
import CultureLogo from "../../Assets/img/kulture_logo.png";
import MotorizationLogo from "../../Assets/img/motor_logo.png";
import ProgrammingLogo from "../../Assets/img/programming_logo.png";
import HistoryLogo from "../../Assets/img/histori_logo.png";
import questionsJson from "./qestionData.json";
interface CategoryData {
    id: number;
    name: keyof typeof  questionsJson;
    logo: string;
    text: string;

}
const allCategoriesData:CategoryData[] = [
    {id: 1, name: "technology", logo: TechnologyLogo, text: "Technology"},
    {id: 2, name: "culture", logo: CultureLogo, text: "Culture"},
    {id: 3, name: "physics", logo: MotorizationLogo, text: "Physics"},
    {id: 4, name: "programming", logo: ProgrammingLogo, text: "Programming"},
    {id: 5, name: "history", logo: HistoryLogo, text: "History"},
];
export default allCategoriesData;